# Alpine Linux with OpenJDK JRE
FROM openjdk:8-jre-alpine
# copy WAR into image
COPY target/update-memberaccount-detail.jar /update-memberaccount-detail.jar
# run application with this command line[
CMD ["java", "-jar", "/update-memberaccount-detail.jar"]
